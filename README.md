# Kubjob

A simple k8s client to run kubernetes jobs in a easy way without having to separatly launch, follow logs, check success conditions and cleanup with complex kubectl workflows

Basically pretends the job is a local process and hides the complexity behind a single command line execution

