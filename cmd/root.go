/*
 * Copyright 2019 Josselin Pujo
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package cmd

import (
	"bytes"
	"context"
	"fmt"
	"github.com/google/gops/agent"
	log "github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"io/ioutil"
	"k8s.io/client-go/kubernetes"
	_ "k8s.io/client-go/plugin/pkg/client/auth"
	"k8s.io/client-go/tools/clientcmd"
	"kubjob/kubutils"
	"os"
	"os/signal"
	"path/filepath"
	"runtime"
	"syscall"
	"time"
)

var debug bool
var kubconfig string

var timeoutS uint32

var rootCmd = &cobra.Command{
	Use:     "kubjob [yamlFile]",
	Short:   "A simple k8s client to run kubernetes jobs",
	Version: "0.2.3",
	Args:    cobra.ExactArgs(1),
	PreRun: func(cmd *cobra.Command, args []string) {
		if debug {
			log.SetLevel(log.DebugLevel)
		} else {
			log.SetLevel(log.InfoLevel)
		}
	},
	Run: func(cmd *cobra.Command, args []string) {
		if err := agent.Listen(agent.Options{}); err != nil {
			log.Fatal(err)
		}
		err := doJob(args[0])
		if err != nil {
			log.WithError(err).Error()
			errorCode, ok := err.(ExitError)
			if ok {
				os.Exit(int(errorCode))
			} else {
				os.Exit(1)
			}
		}
	},
}

func doJob(yamlFileName string) error {
	// Force 0 paralellism to reduce cache chatter and high select costs
	runtime.GOMAXPROCS(1)
	ctx, cancel := context.WithTimeout(context.Background(), time.Duration(timeoutS)*time.Second)
	sigs := make(chan os.Signal, 1)
	signal.Notify(sigs, syscall.SIGINT, syscall.SIGTERM)
	go func() {
		s := <-sigs
		if s != nil {
			log.WithField("Signal", s).Info("Received system signal, cancelling execution")
			cancel()
		}
	}()
	var jobDesc []byte
	var err error
	if yamlFileName == "-" {
		jobDesc, err = ioutil.ReadAll(os.Stdin)
	} else {
		jobDesc, err = ioutil.ReadFile(yamlFileName)
	}
	if err != nil {
		log.WithError(err).Error("Reading yaml file")
		return err
	}
	if kubconfig == "" {
		// Check env variable
		kubConfigEnv, ok := os.LookupEnv("KUBECONFIG")
		if ok {
			kubconfig = kubConfigEnv
		} else {
			if home := homeDir(); home != "" {
				kubconfig = filepath.Join(home, ".kube", "config")
			} else {
				log.Error("No home dir, kubeconfig flag is mandatory")
				return err
			}
		}
	}
	config, err := clientcmd.BuildConfigFromFlags("", kubconfig)
	if err != nil {
		log.WithError(err).Error("Loading k8s configuration")
		return err
	}
	client, err := kubernetes.NewForConfig(config)
	if err != nil {
		log.WithError(err).Error("Creating kubernetes client")
		return err
	}
	log.WithField("client", client).Debug("Created k8s client")
	job, err := kubutils.CreateJob(ctx, bytes.NewReader(jobDesc), client)
	if err != nil {
		log.WithError(err).Error("Starting job")
		return err
	}
	defer func() {
		err := job.Close()
		if err != nil {
			log.WithError(err).Error("Stopping pod")
		}
	}()
	err = job.WaitForPodRunning(ctx)
	if err != nil {
		log.WithError(err).Error("Waiting for pod to run")
		return err
	}
	log.WithField("pod", job.Pod().Name).Info("Started pod")
	err = job.CopyLogs(ctx, os.Stdout)
	if err != nil {
		log.WithError(err).Error("Copying logs to stdout")
	}
	exitCode, err := job.GetExitCode(ctx)
	if err != nil {
		return err
	} else {
		if exitCode != 0 {
			return ExitError(exitCode)
		}
		return nil
	}
}

type ExitError int

func (e ExitError) Error() string {
	return fmt.Sprintf("Exit Code: %d", e)
}

func homeDir() string {
	if h := os.Getenv("HOME"); h != "" {
		return h
	}
	return os.Getenv("USERPROFILE") // windows
}

func init() {
	rootCmd.PersistentFlags().BoolVarP(&debug, "verbose", "v", false, "Verbose Logging")
	rootCmd.PersistentFlags().StringVarP(&kubconfig, "kubeconfig", "k", "", "path to kubeconfig file")
	rootCmd.PersistentFlags().Uint32VarP(&timeoutS, "timeout", "t", 60*15, "timeout in seconds")
}

func Execute() {
	if err := rootCmd.Execute(); err != nil {
		log.Error(err.Error())
	}
}
