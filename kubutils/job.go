/*
 * Copyright 2019 Josselin Pujo
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package kubutils

import (
	"context"
	"errors"
	log "github.com/sirupsen/logrus"
	"io"
	batchv1 "k8s.io/api/batch/v1"
	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/util/yaml"
	k8s "k8s.io/client-go/kubernetes"
	"os"
	rd "runtime/debug"
	"sync"
)

type Job struct {
	Job     *batchv1.Job
	podLock sync.RWMutex
	events  chan *corev1.Pod
	pod     *corev1.Pod
	Client  *k8s.Clientset
}

func (j *Job) Pod() *corev1.Pod {
	j.podLock.RLock()
	defer j.podLock.RUnlock()
	return j.pod
}

func CreateJob(ctx context.Context, jobDescriptor io.Reader, client *k8s.Clientset) (*Job, error) {
	var job = batchv1.Job{}
	decoder := yaml.NewYAMLOrJSONDecoder(jobDescriptor, 8192)
	err := decoder.Decode(&job)
	if err != nil {
		log.WithError(err).Error("Parsing job")
		return nil, err
	}
	log.WithField("job", job).Debug("Parsed job")

	events := make(chan *corev1.Pod, 100)

	go func() {
	OuterLoop:
		for {
			var watchTimoutSeconds int64 = 60 * 5
			watch, err := client.CoreV1().Pods(job.Namespace).Watch(metav1.ListOptions{
				Watch:          true,
				LabelSelector:  "job-name=" + job.Name,
				TimeoutSeconds: &watchTimoutSeconds,
			})
			if err != nil {
				log.WithError(err).Error("Running watch")
			} else {
			InnerLoop:
				for {
					select {
					case e, ok := <-watch.ResultChan():
						if !ok {
							log.Debug("Restarting watch")
							break InnerLoop
						} else {
							pod, ok := e.Object.(*corev1.Pod)
							if ok {
								events <- pod
							}
						}
					case <-ctx.Done():
						break OuterLoop
					}

				}
			}

		}
	}()
	startedJob, err := client.BatchV1().Jobs(job.Namespace).Create(&job)
	if err != nil {
		log.WithError(err).Error("Starting job")
		return nil, err
	}
	log.WithField("job", startedJob.Name).Info("Started job")
	return &Job{
		Job:     &job,
		podLock: sync.RWMutex{},
		events:  events,
		Client:  client,
	}, nil
}

func (j *Job) WaitForPodRunning(ctx context.Context) error {
	pod := j.Pod()
	if pod != nil {
		phase := j.Pod().Status.Phase
		if phase == corev1.PodRunning || phase == corev1.PodSucceeded || phase == corev1.PodFailed {
			return nil
		}
	}
	for {
		select {
		case pod := <-j.events:
			{
				log.WithField("pod", pod.Name).WithField("phase", pod.Status.Phase).Debug("pod event received")
				j.podLock.Lock()
				j.pod = pod
				j.podLock.Unlock()
				if pod.Status.Phase == corev1.PodRunning || pod.Status.Phase == corev1.PodSucceeded || pod.Status.Phase == corev1.PodFailed {
					return nil
				}
			}
		case <-ctx.Done():
			{
				return errors.New("timeout waiting for pod creation")
			}
		}
	}

}

func (j *Job) CopyLogs(ctx context.Context, output io.Writer) error {
	err := j.WaitForPodRunning(ctx)
	if err != nil {
		return err
	}
	pod := j.Pod()
	log.WithField("pod", pod.Name).Debug("Requesting pod logs")
	var goBack int64 = 60 * 5
	logRequest := j.Client.CoreV1().Pods(pod.Namespace).GetLogs(pod.Name, &corev1.PodLogOptions{
		Follow:       true,
		SinceSeconds: &goBack,
	})
	reader, err := logRequest.Stream()
	if err != nil {
		return err
	}
	res := make(chan error)
	go func() {
		_, err = io.CopyBuffer(os.Stdout, reader, []byte{0})
		res <- err
	}()
	select {
	case r := <-res:
		return r
	case <-ctx.Done():
		return nil
	}
}

func (j *Job) GetExitCode(ctx context.Context) (code int32, err error) {
	defer func() {
		if r := recover(); r != nil {
			log.WithField("in", r).WithField("stack", string(rd.Stack())).Error("Recovered from panic GetExitCode")
			code = 3
			err = errors.New("panic in GetExitCode")
		}
	}()
	pod := j.Pod()
	phase := pod.Status.Phase
	if phase == corev1.PodSucceeded || phase == corev1.PodFailed {
		return pod.Status.ContainerStatuses[0].State.Terminated.ExitCode, nil
	}
	for {
		select {
		case <-ctx.Done():
			{
				err := errors.New("timeout waiting for pod termination")
				log.Debug(err.Error())
				return 2, err
			}
		case pod = <-j.events:
			{
				phase = pod.Status.Phase
				if phase == corev1.PodSucceeded || phase == corev1.PodFailed {
					return pod.Status.ContainerStatuses[0].State.Terminated.ExitCode, nil
				}
			}
		}
	}
}

func (j *Job) Close() error {
	log.WithField("job", j.Job.Name).Info("Cleaning up")
	deletePolicy := metav1.DeletePropagationForeground
	gracePeriod := int64(10)
	err := j.Client.BatchV1().Jobs(j.Job.Namespace).Delete(j.Job.Name, &metav1.DeleteOptions{
		GracePeriodSeconds: &gracePeriod,
		PropagationPolicy:  &deletePolicy,
	})
	if err != nil {
		log.WithError(err).Error("Deleting job")
	} else {
		log.WithField("job", j.Job.Name).Info("Job deleted")
	}
	return err
}
